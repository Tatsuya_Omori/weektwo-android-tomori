package com.example.weektwo

import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.PolyUtil
import org.json.JSONArray
import java.util.*
import java.util.Locale


class MapsFragment : Fragment(), OnMapReadyCallback {
    private var address1 = ""
    private var address2 = ""
    private var latLngOrigin = LatLng(0.0, 0.0)
    private var latLngDestination = LatLng(0.0, 0.0)
    private var mMap: GoogleMap? = null

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap ?: return
        direction()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        setFragmentResultListener("request_key") { _, bundle ->
            address1 = bundle.getString("result_key1").toString()
            address2 = bundle.getString("result_key2").toString()
        }

        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    private  fun direction(){
        val latitude1 = geoCoding(address1).first
        val longitude1 = geoCoding(address1).second

        val latitude2 = geoCoding(address2).first
        val longitude2 = geoCoding(address2).second

        latLngOrigin = LatLng(latitude1, longitude1) //始点
        latLngDestination = LatLng(latitude2, longitude2) //終点

        //Route
        val path: MutableList<List<LatLng>> = ArrayList()
        val urlDirections = "https://sonix-training-api.herokuapp.com/maps/directions?origin_lat=${latitude1}&origin_lng=${longitude1}&destination_lat=${latitude2}&destination_lng=${longitude2}&mode=walking"
        val directionsRequest = MyStringRequest(Request.Method.GET, urlDirections, { response ->
            val jsonResponse = JSONArray(response)
            val legs = jsonResponse.getJSONObject(0).getJSONArray("legs")
            val steps = legs.getJSONObject(0).getJSONArray("steps")
            for (i in 0 until steps.length()) {
                val points = steps.getJSONObject(i).getJSONObject("polyline").getString("points")
                path.add(PolyUtil.decode(points))
            }
            for (i in 0 until path.size) {
                mMap?.addPolyline(PolylineOptions().addAll(path[i]).color(Color.RED))
            }
        }, {
        })
        val requestQueue = Volley.newRequestQueue(requireContext())
        requestQueue.add(directionsRequest)

        mMap?.addMarker(MarkerOptions().position(latLngOrigin).title(address1))
        mMap?.addMarker(MarkerOptions().position(latLngDestination).title(address2))
        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngOrigin, 14.5f))

    }

    private fun geoCoding(address: String): Pair<Double, Double> {
        val gcoder = Geocoder(requireContext(), Locale.getDefault())
        // 位置情報の取得
        val lstAddr: List<Address> = gcoder.getFromLocationName(address, 1)
        if (lstAddr.isNotEmpty()) {
            // 緯度・経度取得
            val addr = lstAddr[0]
            val latitude = addr.latitude
            val longitude = addr.longitude

            return Pair(latitude, longitude)
        }
        return Pair(0.0, 0.0)
    }
}

class MyStringRequest(method: Int,
                      url: String?,
                      listener: Response.Listener<String?>?,
                      errorListener: Response.ErrorListener?) : StringRequest(method, url, listener, errorListener) {
    companion object {
        // タイムアウト値（5000ミリ秒）
        private const val TIMEOUT_MS = 5000

        // リトライ回数（初回リクエスト失敗後、3回リトライ）
        private const val MAX_RETRIES = 3
    }

    init {

        // タイムアウト値、リトライ回数をリトライポリシーに設定する
        retryPolicy = DefaultRetryPolicy(TIMEOUT_MS,
                MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
    }
}