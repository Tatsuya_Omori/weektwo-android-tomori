package com.example.weektwo

import android.app.Activity
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import com.example.weektwo.databinding.FragmentTopBinding
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase


class TopFragment : Fragment() {
    private var binding: FragmentTopBinding? = null
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        firebaseAnalytics = Firebase.analytics
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTopBinding.inflate(inflater, container, false)

        binding?.editTextStart?.setOnKeyListener(object : View.OnKeyListener {
            //コールバックとしてonKey()メソッドを定義
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                //イベントを取得するタイミングには、ボタンが押されてなおかつエンターキーだったときを指定
                if (event.action === KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    //キーボードを閉じる
                    val manager = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    manager.hideSoftInputFromWindow(view?.windowToken, 0)
                    return true
                }
                return false
            }
        })

        binding?.search?.setOnClickListener(){
            if (binding?.editTextStart?.text.toString() != "" && binding?.editTextEnd?.text.toString() != ""){
                var start = binding?.editTextStart?.text
                var end = binding?.editTextEnd?.text
                setFragmentResult("request_key", bundleOf(
                        "result_key1" to start.toString(),
                        "result_key2" to end.toString()
                ))
                findNavController().navigate(R.id.action_topFragment_to_mapsFragment)

                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                    param(FirebaseAnalytics.Param.ITEM_NAME, "検索")
                }
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SEARCH) {
                    param(FirebaseAnalytics.Param.ITEM_CATEGORY, binding?.editTextStart?.text.toString())
                    param(FirebaseAnalytics.Param.ITEM_CATEGORY2, binding?.editTextEnd?.text.toString())
                }
            }else{
                Toast.makeText(requireContext(), "未入力の項目があります", Toast.LENGTH_SHORT).show()
            }
        }
        val view = binding?.root
        return view
    }
}

